from logging import Logger

from streambrain.clients.irc import IRCClient
from streambrain.listeners.irc import (
        IRCClientMessageEvent, IRCClientPingEvent,
        IRCClientPrivateMessageEvent)
from streambrain.streambrain import Handler


class IRCClientMessageHandler(Handler):
    def __init__(self, logger: Logger = None):
        super().__init__("irc_client_message", logger)


class IRCClientPingHandler(Handler):
    def __init__(self, irc_client: IRCClient, logger: Logger = None):
        super().__init__("irc_client_ping", logger)
        self.irc_client = irc_client

    async def handle(self, ping_event: IRCClientPingEvent):
        if ping_event["client_id"] != self.irc_client.client_id:
            return
        await self.irc_client.pong(ping_event["irc_message"]["parameters"])


class IRCClientPrivateMessageHandler(Handler):
    def __init__(self, logger: Logger = None):
        super().__init__("irc_client_private_message", logger)
