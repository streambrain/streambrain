from asyncio import TaskGroup, CancelledError
from asyncio import Event as AsyncioEvent
from datetime import datetime
from json import dumps as dump_json_str
from logging import getLogger, Logger

from websockets.server import serve, ServerConnection


module_logger = getLogger(__name__)


class Event(dict):
    def __init__(self, event_name: str = "streambrain_event") -> None:
        self["event_name"] = event_name
        self["event_created_at"] = datetime.now()


class Handler:
    def __init__(
            self, handles_event_name: str, logger: Logger = None) -> None:
        self.handles_event_name = handles_event_name
        if logger is None:
            logger = module_logger.getChild(type(self).__name__)
        self.logger = logger

    async def handle(self, streambrain_event: Event) -> None:
        pass


class Listener:
    def __init__(self, logger: Logger = None) -> None:
        if logger is None:
            logger = module_logger.getChild(type(self).__name__)
        self.logger = logger
        self._is_listening = False

    async def listen(self, handle_callback: callable) -> None:
        for streambrain_event in await self.get_events():
            await handle_callback(streambrain_event)

    async def listen_forever(self, handle_callback: callable) -> None:
        self._is_listening = True
        while self._is_listening:
            try:
                await self.listen(handle_callback)
            except CancelledError:
                await self.stop()
                raise

    async def stop(self) -> None:
        self._is_listening = False
        await self.shutdown()

    async def get_events(self) -> list[Event]:
        return []

    async def shutdown(self) -> None:
        pass


class StreamBrain:
    def __init__(self, logger: Logger = None) -> None:
        if logger is None:
            logger = module_logger.getChild(type(self).__name__)
        self.logger = logger 
        self.event_name_handler_map = {}
        self.listener_task_map = {}

    def apply_handler(self, handler: Handler) -> None:
        handles_event_name = handler.handles_event_name
        if handles_event_name not in self.event_name_handler_map:
            self.event_name_handler_map[handles_event_name] = []
        self.event_name_handler_map[handles_event_name].append(handler)

    def apply_listener(self, listener: Listener) -> None:
        if listener not in self.listener_task_map:
            self.listener_task_map[listener] = None

    def stop(self) -> None:
        for listener in self.listener_task_map:
            self.listener_task_map[listener].cancel()
            self.listener_task_map[listener] = None

    async def handle(self, event: Event) -> None:
        if event["event_name"] in self.event_name_handler_map:
            for handler in self.event_name_handler_map[event["event_name"]]:
                await handler.handle(event)

    async def run(self) -> None:
        async with TaskGroup() as tg:
            for listener in self.listener_task_map:
                task = tg.create_task(listener.listen_forever(self.handle))
                self.listener_task_map[listener] = task


class WebSocketStreamBrain(StreamBrain):
    def __init__(
            self, hostname: str, port: int, logger: Logger = None) -> None:
        self.hostname = hostname
        self.port = port
        self.connections = []
        super().__init__(logger)

    async def apply_websocket(self, connection: ServerConnection) -> None:
        self.connections.append(connection)
        await connection.wait_closed()

    async def handle(self, event: Event) -> None:
        for connection in self.connections:
            await connection.send(dump_json_str(event))
        await super().handle(event)

    async def run(self) -> None:
        async with serve(self.apply_websocket, self.hostname, self.port):
            await super().run()
