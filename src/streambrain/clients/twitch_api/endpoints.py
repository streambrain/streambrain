import json
import urllib.parse
import urllib.request

import requests

from typing import List, Optional, Tuple, Union

from .objects import (
        TwitchHTTPError, TwitchScheduleSegment, TwitchStreamData,
        TwitchUser, _construct_schedule_segment_from_raw_dict,
        _construct_stream_data_from_raw_dict,
        _construct_streams_query_parameters, _construct_user_from_raw_dict)


API_URL = "https://api.twitch.tv/helix/"
REFRESH_URL = "https://id.twitch.tv/oauth2/token"


def _call_api(
        access_token: str, client_id: str, endpoint: str,
        query_parameters: List[Tuple[str, Union[int, str]]]) -> dict:
    encoded_parameters = urllib.parse.urlencode(query_parameters)
    request_url = f"{API_URL}{endpoint}?{encoded_parameters}"
    request_headers = {
            "Authorization": f"Bearer {access_token}",
            "Client-Id": client_id}
    request = urllib.request.Request(request_url, None, request_headers)
    try:
        with urllib.request.urlopen(request) as response:
            return json.load(response)
    except urllib.error.HTTPError as e:
        raise TwitchHTTPError(e, endpoint, query_parameters)


def _call_api_paginated(
        access_token: str, client_id: str, endpoint: str,
        query_parameters: List[Tuple[str, Union[int, str]]],
        paginated_key: Optional[str]=None, page_size: Optional[int]=None,
        max_pages: Optional[int]=None,
        after: Optional[str]=None) -> Tuple[list, dict, str]:
    total_data = []
    total_parameters = query_parameters
    if after:
        total_parameters.append(("after", after))
    page_counter = 0
    while page_counter < max_pages if max_pages else True:
        response_data = _call_api(
                access_token, client_id, endpoint, total_parameters)
        page_counter += 1
        if paginated_key:
            total_data += response_data["data"][paginated_key]
        else:
            total_data += response_data["data"]
        if response_data["pagination"]:
            cursor = response_data["pagination"]["cursor"]
            total_parameters = query_parameters + [("after", cursor)]
        else:
            cursor = None
            break
    if paginated_key:
        del response_data["data"][paginated_key]
        metadata = response_data["data"]
    else:
        metadata = {}
    return total_data, metadata, cursor


def get_channel_stream_schedule(
        access_token: str, client_id: str, broadcaster_id: int,
        segment_ids: List[str]=[], start_time: Optional[str]=None,
        page_size: Optional[int]=None, max_pages: Optional[int]=None,
        after: Optional[str]=None) \
                -> Tuple[List[TwitchScheduleSegment], dict, str]:
    parameters = [("broadcaster_id", broadcaster_id)]
    for segment_id in segment_ids:
        parameters.append(("id", segment_id))
    if start_time:
        parameters.append(("start_time", start_time))
    segments_data, metadata, cursor = _call_api_paginated(
            access_token, client_id, "schedule", parameters, "segments",
            page_size, max_pages, after)
    segments = []
    for segment_data in segments_data:
        segment = _construct_schedule_segment_from_raw_dict(segment_data)
        segments.append(segment)
    return segments, metadata, cursor


def get_streams(
        access_token: str, client_id: str, user_ids: List[int]=[],
        user_logins: List[str]=[], game_ids: List[int]=[],
        stream_type: Optional[str]=None, language: Optional[str]=None,
        page_size: Optional[int]=None, max_pages: Optional[int]=None,
        after: Optional[str]=None) -> Tuple[List[TwitchStreamData], str]:
    query_parameters = _construct_streams_query_parameters(
            user_ids, user_logins, game_ids, stream_type, language,
            page_size)
    streams_data, metadata, cursor = _call_api_paginated(
            access_token, client_id, "streams", query_parameters, None,
            page_size, max_pages, after)
    streams = []
    for stream_data in streams_data:
        stream_data = _construct_stream_data_from_raw_dict(stream_data)
        streams.append(stream_data)
    return streams, cursor


def get_users(
        access_token: str, client_id: str, user_ids: List[int]=[],
        logins: List[str]=[]) -> dict:
    parameters = []
    for user_id in user_ids:
        parameters.append(("id", user_id))
    for login in logins:
        parameters.append(("login", login))
    users_data = _call_api(
            access_token, client_id, "users", parameters)
    users = []
    for user_data in users_data["data"]:
        users.append(_construct_user_from_raw_dict(user_data))
    return users


def refresh_access_token(
        client_id: str, client_secret: str, refresh_token: str) -> str:
    parameters = {
            "client_id": client_id, "client_secret": client_secret,
            "grant_type": "refresh_token", "refresh_token": refresh_token}
    #refresh_data = bytes(urllib.parse.urlencode(parameters), "ASCII")
    #request = urllib.request.Request(REFRESH_URL, refresh_data)
    #with urllib.request.urlopen(request) as response:
    #    response_data = json.load(response)
    response = requests.post(REFRESH_URL, data=parameters)
    response_data = json.loads(response.text)
    if "error" in response_data:
        if response_data["message"] == "Invalid refresh token":
            raise InvalidRefreshTokenError(refresh_token)
    return response_data
