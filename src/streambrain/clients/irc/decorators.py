from asyncio import sleep


class IRCClientNotConnectedError(Exception):
    pass


class IRCClientDisconnectedError(Exception):
    def __init__(self, disconnected_client: "IRCClient"):
        self.client = disconnected_client
        super().__init__()


class RemoteConnectionClosedError(Exception):
    pass


DISCONNECTED_ERRORS = [
        ConnectionAbortedError, ConnectionResetError, TimeoutError,
        RemoteConnectionClosedError]


def need_connection(to_decorate: callable) -> callable:
    async def decorated(irc_client: "IRCClient", *args, **kwargs):
        if irc_client.write_stream is None:
            irc_client.logger.warning(
                    "a method requiring an IRC connection cannot run "
                    "because there is no connection.")
            raise IRCClientNotConnectedError
        try:
            return await to_decorate(irc_client, *args, **kwargs)
        except tuple(DISCONNECTED_ERRORS) as e:
            irc_client.logger.warning(
                    "a method requiring an IRC connection received a "
                    "disconnection error.")
            raise IRCClientDisconnectedError(irc_client)
    return decorated


def reconnect_and_retry(to_decorate: callable) -> callable:
    async def decorated(irc_client: "IRCClient", *args, **kwargs):
        while True:
            try:
                return_value = await to_decorate(
                        irc_client, *args, **kwargs)
            except IRCClientDisconnectedError:
                irc_client.logger.warning(
                        f"will retry after {irc_client.retry_seconds}")
                await sleep(irc_client.retry_seconds)
                irc_client.retry_seconds *= 2
                await irc_client.disconnect()
                await irc_client.connect((irc_client.host, irc_client.port))
                await irc_client.login(
                        irc_client.password, irc_client.nickname,
                        irc_client.username)
                for capability_name in irc_client.requested_capabilities:
                    await irc_client.request_capability(capability_name)
                for channel_name in irc_client.channels:
                    await irc_client.join(channel_name)
            else:
                irc_client.retry_seconds = irc_client.default_retry_seconds
                return return_value
    return decorated
