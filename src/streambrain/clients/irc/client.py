from asyncio import IncompleteReadError, open_connection
from logging import getLogger, Logger

from streambrain.clients.irc.decorators import (
        need_connection, reconnect_and_retry, RemoteConnectionClosedError)
from streambrain.clients.irc.message import IRCMessage


module_logger = getLogger(__name__)


class IRCClient:
    def __init__(
            self, client_id: str = "default", logger: Logger = None,
            default_retry_seconds: int = 1) -> None:
        if logger is None:
            logger = module_logger.getChild(type(self).__name__)
        self.client_id = client_id
        self.logger = logger
        self.default_retry_seconds = default_retry_seconds
        self.retry_seconds = default_retry_seconds
        self.host = None
        self.port = None
        self.read_stream = None
        self.write_stream = None
        self.password = None
        self.nickname = None
        self.username = None
        self.requested_capabilities = []
        self.channels = []

    async def connect(self, address: tuple[str, int]) -> None:
        self.host, self.port = address
        if self.write_stream is not None:
            self.logger.info("closing previous connection")
            await self.disconnect()
        self.logger.info(f"connecting to {address[0]} on port {address[1]}")
        streams = await open_connection(self.host, self.port)
        self.read_stream, self.write_stream = streams

    async def disconnect(self) -> None:
        if self.write_stream is not None:
            self.logger.info("disconnecting")
            self.write_stream.close()
            try:
                await self.write_stream.wait_closed()
            except ConnectionResetError:
                self.logger.warning(
                        "got ConnectionResetError while waiting for socket "
                        "to close, probably because we were already "
                        "disconnected")
            self.write_stream = None

    @reconnect_and_retry
    @need_connection
    async def send_irc_message(self, irc_message: str) -> None:
        self.logger.info(f"[SEND] {irc_message}")
        self.write_stream.write(f"{irc_message}\r\n".encode())
        await self.write_stream.drain()

    @reconnect_and_retry
    @need_connection
    async def login(
            self, password: str, nickname: str,
            username: str = None) -> None:
        self.password = password
        self.nickname = nickname
        self.username = username
        self.logger.info("beginning authentication")
        if password is not None:
            # don't self.send_irc_message because that would log password
            self.logger.info("sending password")
            self.write_stream.write(f"PASS {password}\r\n".encode())
            await self.write_stream.drain()
        if nickname is not None:
            await self.send_irc_message(f"NICK {nickname}")
        if username is not None:
            await self.send_irc_message(f"USER {username}")

    @reconnect_and_retry
    @need_connection
    async def request_capability(self, capability_name: str) -> None:
        await self.send_irc_message(f"CAP REQ :{capability_name}")
        if capability_name not in self.requested_capabilities:
            self.requested_capabilities.append(capability_name)

    @reconnect_and_retry
    @need_connection
    async def pong(self, parameters: list[str]) -> None:
        parameters_str = ""
        for parameter in parameters:
            if parameter.startswith(":"):
                # escape leading ':' with another ':'
                parameter = f":{parameter}"
            if parameter.count(" "):
                # parameters w/ spaces must be preceded with ':'
                # and must be sent LAST, but we don't check for that
                parameter = f":{parameter}"
            parameters_str += f" {parameter}"
        await self.send_irc_message(f"PONG{parameters_str}")

    @reconnect_and_retry
    @need_connection
    async def join(self, channel_name: str) -> None:
        await self.send_irc_message(f"JOIN #{channel_name}")
        if channel_name not in self.channels:
            self.channels.append(channel_name)

    @reconnect_and_retry
    @need_connection
    async def part(self, channel_name: str) -> None:
        await self.send_irc_message(f"PART #{channel_name}")
        try:
            self.channels.remove(channel_name)
        except ValueError:
            self.logger.warning(
                    "parted from '{channel_name}' but it wasn't in list of "
                    "joined channels")
            pass

    @reconnect_and_retry
    @need_connection
    async def private_message(
            self, channel_name: str, message_str: str) -> None:
        await self.send_irc_message(
                f"PRIVMSG #{channel_name} :{message_str}")

    @reconnect_and_retry
    @need_connection
    async def read_message(self) -> IRCMessage:
        self.logger.info("reading incoming stream for one IRC message")
        try:
            recv_data = await self.read_stream.readuntil("\r\n".encode())
        except IncompleteReadError:
            raise RemoteConnectionClosedError
        recv_str = recv_data.decode("utf-8", "replace")
        self.logger.info(f"[RECV] {recv_str}".rstrip("\r\n"))
        return IRCMessage(recv_str)
