from re import compile as re_compile


IRC_MESSAGE_REGEX = re_compile(
        "^(@(?P<tags>.*?) )?(:(?P<prefix>.*?) )?(?P<command>.*?)"
        "( (?P<parameters>.*?))?\r\n$")


class IRCMessage(dict):
    def __init__(self, raw_message: str) -> None:
        match = IRC_MESSAGE_REGEX.fullmatch(raw_message)
        self["command"] = match.group("command")
        self["tags"] = {}
        if match.group("tags"):
            for tag in match.group("tags").split(";"):
                tag_key, tag_value = tag.split("=", 1)
                self["tags"][tag_key] = tag_value
        self["prefix"] = match.group("prefix")
        if match.group("parameters"):
            param_colon_split = match.group("parameters").split(":")
            self["parameters"] = param_colon_split[0].split()
            if len(param_colon_split) > 1:
                self["parameters"].append(":".join(param_colon_split[1:]))
        else:
            self["parameters"] = []
