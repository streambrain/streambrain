import json
import urllib.parse
import urllib.request

from dataclasses import dataclass
from typing import List, Tuple, Union


API_URL = "https://api.givebutter.com/v1/"


@dataclass
class GivingSpace:
    giving_space_id: int
    donor_name: str
    amount: int
    message: str


@dataclass
class Transaction:
    transaction_id: str
    transaction_number: str
    campaign_id: int
    campaign_code: int
    plan_id: int
    team_id: int
    member_id: int
    fund_id: str
    fund_code: str
    first_name: str
    last_name: str
    email: str
    phone: str
    address: dict
    communication_opt_in: bool
    status: str
    method: str
    amount: float
    fee: float
    fee_covered: float
    donated: float
    payout: float
    currency: str
    transacted_at: str
    created_at: str
    captured: bool
    captured_at: str
    refunded: bool
    refunded_at: str
    line_items: list
    giving_space: GivingSpace
    utm_paramaters: dict
    attribution_data: list


def _call_api(
        api_key: str, endpoint: str,
        query_parameters: List[Tuple[str, Union[int, str]]]):
    encoded_parameters = urllib.parse.urlencode(query_parameters)
    request_url = f"{API_URL}{endpoint}?{encoded_parameters}"
    # Givebutter bug: API requests fail without a User-Agent header.
    request_headers = {
            "Authorization": f"Bearer {api_key}",
            "User-Agent": "Fuck You."}
    request = urllib.request.Request(request_url, None, request_headers)
    with urllib.request.urlopen(request) as response:
        response_dict = json.load(response)
    is_multi_page = "links" in response_dict
    if not is_multi_page:
        return [response_dict]
    total_data = response_dict["data"]
    while response_dict["links"]["next"]:
        request_url = response_dict["links"]["next"]
        request = urllib.request.Request(request_url, None, request_headers)
        with urllib.request.urlopen(request) as response:
            response_dict = json.load(response)
        total_data += response_dict["data"]
    return total_data


def get_transactions(api_key: str) -> List[Transaction]:
    query_params = [("scope", "null")]
    response_data = _call_api(api_key, "transactions", query_params)
    transactions = []
    for data in response_data:
        currency = data["currency"]
        if not data["giving_space"]:
            # This is a bug I've experienced rarely. Sometimes,
            # especially immediately following a donation, a transaction
            # may be served up by the API which has 'None' for the
            # giving_space object. Because this bug is rare and I'd like
            # to diagnose it, we should capture it.
            # For now, let's just not return transactions until (?) they
            # have a giving_space.
            continue
        gs = data["giving_space"]
        giving_space = GivingSpace(
                gs["id"], gs["name"], gs["amount"], gs["message"])
        transaction = Transaction(
                transaction_id=data["id"],
                transaction_number=data["number"],
                campaign_id=data["campaign_id"],
                campaign_code=data["campaign_code"],
                plan_id=data["plan_id"],
                team_id=data["team_id"],
                member_id=data["member_id"],
                fund_id=data["fund_id"],
                fund_code=data["fund_code"],
                first_name=data["first_name"],
                last_name=data["last_name"],
                email=data["email"],
                phone=data["phone"],
                address=data["address"],
                communication_opt_in=data["communication_opt_in"],
                status=data["status"],
                method=data["method"],
                amount=data["amount"],
                fee=data["fee"],
                fee_covered=data["fee_covered"],
                donated=data["donated"],
                payout=data["payout"],
                currency=data["currency"],
                transacted_at=data["transacted_at"],
                created_at=data["created_at"],
                captured=data["transactions"][0]["captured"],
                captured_at=data["transactions"][0]["captured_at"],
                refunded=data["transactions"][0]["refunded"],
                refunded_at=data["transactions"][0]["refunded_at"],
                line_items=data["transactions"][0]["line_items"],
                giving_space=giving_space,
                utm_paramaters=data["utm_parameters"],
                attribution_data=data["attribution_data"])
        transactions.append(transaction)
    return transactions
