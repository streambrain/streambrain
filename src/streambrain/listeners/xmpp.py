from asyncio import Event as AsyncioEvent
from logging import Logger

from streambrain.streambrain import Event, Listener
from streambrain.clients.xmpp import Component


class XMPPGroupMessageEvent(Event):
    def __init__(
            self, from_jid: str, to_jid: str, body_text: str,
            message_id: str, from_nickname: str) -> None:
        super().__init__("xmpp_group_message")
        self["from_jid"] = from_jid
        self["to_jid"] = to_jid
        self["body_text"] = body_text
        self["message_id"] = message_id
        self["from_nickname"] = from_nickname


class XMPPMUCListener(Listener):
    def __init__(
            self, xmpp_component: Component, logger: Logger = None) -> None:
        super().__init__(logger)
        self.component = xmpp_component
        self._handle_callback = None

    async def listen(self, handle_callback: callable) -> None:
        self._handle_callback = handle_callback
        self.component.add_event_handler(
                "groupchat_message", self.handle_as_streambrain_event)
        self._is_listening = True
        await AsyncioEvent().wait()

    async def shutdown(self) -> None:
        self.component.del_event_handler(
                "groupchat_message", self.handle_as_streambrain_event)
        self._handle_callback = None

    async def handle_as_streambrain_event(self, message: dict) -> None:
        streambrain_event = XMPPGroupMessageEvent(
                message["from"].bare, message["to"], message["body"],
                message["id"], message["mucnick"])
        await self._handle_callback(streambrain_event)
