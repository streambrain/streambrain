from logging import Logger

from streambrain.streambrain import Event, Listener
from streambrain.clients.irc import IRCMessage, IRCClient


class IRCClientMessageEvent(Event):
    def __init__(
            self, client_id: str, irc_message: IRCMessage,
            event_name: str = "irc_client_message") -> None:
        super().__init__(event_name)
        self["client_id"] = client_id
        self["irc_message"] = irc_message


class IRCClientPrivateMessageEvent(IRCClientMessageEvent):
    def __init__(
            self, client_id: str, irc_message: IRCMessage) -> None:
        super().__init__(
                client_id, irc_message, "irc_client_private_message")


class IRCClientPingEvent(IRCClientMessageEvent):
    def __init__(
            self, client_id: str, irc_message: IRCMessage) -> None:
        super().__init__(client_id, irc_message, "irc_client_ping")


class IRCClientJoinEvent(IRCClientMessageEvent):
    def __init__(
            self, client_id: str, irc_message: IRCMessage) -> None:
        super().__init__(client_id, irc_message, "irc_client_join")


class IRCClientUserstateEvent(IRCClientMessageEvent):
    def __init__(
            self, client_id: str, irc_message: IRCMessage) -> None:
        super().__init__(client_id, irc_message, "irc_client_userstate")


class IRCClientNoticeEvent(IRCClientMessageEvent):
    def __init__(
            self, client_id: str, irc_message: IRCMessage) -> None:
        super().__init__(client_id, irc_message, "irc_client_notice")


IRC_COMMAND_EVENT_MAP = {
        "PRIVMSG": IRCClientPrivateMessageEvent, "PING": IRCClientPingEvent,
        "JOIN": IRCClientJoinEvent, "USERSTATE": IRCClientUserstateEvent,
        "NOTICE": IRCClientNoticeEvent}


class IRCClientListener(Listener):
    def __init__(
            self, irc_client: IRCClient, logger: Logger = None) -> None:
        super().__init__(logger)
        self.irc_client = irc_client

    async def get_events(self) -> list[Event]:
        irc_message = await self.irc_client.read_message()
        client_id = self.irc_client.client_id
        events = [IRCClientMessageEvent(client_id, irc_message)]
        if irc_message["command"] in IRC_COMMAND_EVENT_MAP:
            EventType = IRC_COMMAND_EVENT_MAP[irc_message["command"]]
            events.append(EventType(client_id, irc_message))
        return events
