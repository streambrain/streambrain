from asyncio import get_event_loop

from streambrain.streambrain import Event, Listener


class InputEvent(Event):
    def __init__(self, user_input: str):
        super().__init__("input_command")
        self["user_input"] = user_input


class InputListener(Listener):
    async def get_events(self) -> list[InputEvent]:
        user_input = await get_event_loop().run_in_executor(None, input)
        return [InputEvent(user_input=user_input)]
