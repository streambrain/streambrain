# streambrain
## Description
A collection of modules for interfacing with remote services that may be useful for a livestreamer or other service.

## Status
As of 2025-30-2024, these modules are mostly working but require the implementation of logging to address some rare bugs (or common crashes in the case of the IRC client).

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
